﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MarketCommon.Models;

namespace FruitManager.Helpers
{
    public static class FileHelper
    {
        static XmlSerializer _xmlSerializer = new XmlSerializer(typeof(List<MarketSale>));

        public static List<MarketSale> LoadSales(string filePath)
        {
            List<MarketSale> salesList = null;

            if (File.Exists(filePath))
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Open))
                {
                    salesList = _xmlSerializer.Deserialize(fs) as List<MarketSale>;
                }
            }

            return salesList;
        }

        public static void SaveSales(string filePath, List<MarketSale> sales)
        {
            using (FileStream fs = new FileStream(filePath, FileMode.Create))
            {
                _xmlSerializer.Serialize(fs, sales);
            }
        }
    }


}
