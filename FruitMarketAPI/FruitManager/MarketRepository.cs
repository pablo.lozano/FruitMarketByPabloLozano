﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using FruitManager.Helpers;
using MarketCommon.Interfaces;
using MarketCommon.Models;
using System.IO;

namespace FruitManager
{
    public class MarketRepository : IMarketRepository
    {
        #region Constants


        readonly string DATABASE_FILE_PATH = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments), "marketdatabase.xml");

        #endregion

        #region Variables

        Fruit _apple = new Fruit() { ID = 1, Name = "Apple", Price = 0.49M };
        Fruit _banana = new Fruit() { ID = 1, Name = "Banana", Price = 0.59M };
        Fruit _orange = new Fruit() { ID = 1, Name = "Orange", Price = 0.99M };
        Fruit _pear = new Fruit() { ID = 1, Name = "Pears", Price = 0.29M };
        Fruit _watermelon = new Fruit() { ID = 1, Name = "Watermelon", Price = 1.49M };


        #endregion


        public IEnumerable<MarketSale> GetSales()
        {
            return FileHelper.LoadSales(DATABASE_FILE_PATH);
        }

        public bool AddSale(MarketSale sale)
        {
            try
            {
                List<MarketSale> salesList = (List<MarketSale>)GetSales();

                salesList.Add(sale);
                FileHelper.SaveSales(DATABASE_FILE_PATH, salesList);

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return false;
            }
        }

        public IEnumerable<Fruit> GetFruits()
        {
            List<Fruit> fruits = new List<Fruit>();

            fruits.Add(_apple);
            fruits.Add(_banana);
            fruits.Add(_orange);
            fruits.Add(_pear);
            fruits.Add(_watermelon);

            return fruits;
        }
    }
}
