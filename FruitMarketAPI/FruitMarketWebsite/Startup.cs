﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FruitMarketWebsite.Startup))]
namespace FruitMarketWebsite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
