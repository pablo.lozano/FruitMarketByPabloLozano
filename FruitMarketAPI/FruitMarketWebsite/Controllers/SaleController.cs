﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using FruitMarketWebsite.Models;

namespace FruitMarketWebsite.Controllers
{
    public class SaleController : Controller
    {
        HttpClient _client;
        const string API_URL = "http://localhost:51460";

        public SaleController()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(API_URL);
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }


        // GET: Sale
        public ActionResult Index()
        {
            HttpResponseMessage responsseMsg = _client.GetAsync("api/Sales").Result;
            var responseData = responsseMsg.Content.ReadAsStringAsync().Result;

            var sales = JsonConvert.DeserializeObject<List<SaleModel>>(responseData);

            HttpResponseMessage responsseMsg2 = _client.GetAsync("api/Fruit").Result;
            var responseData2 = responsseMsg2.Content.ReadAsStringAsync().Result;
            var fruits = JsonConvert.DeserializeObject<List<SaleFruit>>(responseData2);
            ViewData["fruits"] = fruits;

            return View(sales);
        }

        public ActionResult Filter(string startDate, string endDate)
        {
            HttpResponseMessage responsseMsg = _client.GetAsync("api/Sales").Result;
            var responseData = responsseMsg.Content.ReadAsStringAsync().Result;

            var sales = JsonConvert.DeserializeObject<List<SaleModel>>(responseData);

            HttpResponseMessage responsseMsg2 = _client.GetAsync("api/Fruit").Result;
            var responseData2 = responsseMsg2.Content.ReadAsStringAsync().Result;
            var fruits = JsonConvert.DeserializeObject<List<SaleFruit>>(responseData2);

            DateTime startDT = DateTime.Parse(startDate);
            DateTime endDT = DateTime.Parse(startDate);

            bool startDTOK = DateTime.TryParse(startDate, out startDT);
            bool endDTOK = DateTime.TryParse(startDate, out endDT);

            if (startDTOK && endDTOK)
            {
                sales = sales.Where(u => u.SaleDate >= startDT && u.SaleDate <= endDT).OrderBy(u => u.SaleDate).ToList();
                ViewData["fruits"] = fruits;
            }

            return View("Index", sales);
        }
    }
}