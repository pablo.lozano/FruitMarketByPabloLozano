﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FruitMarketWebsite.Models
{
    public class SaleModel
    {
        #region Properties

        public int ID
        { get; set; }

        public DateTime SaleDate
        { get; set; }

        public int Quantity
        { get; set; }

        [JsonProperty("SaleFruit")]
        public SaleFruit Fruit
        { get; set; }

        #endregion
    }
}