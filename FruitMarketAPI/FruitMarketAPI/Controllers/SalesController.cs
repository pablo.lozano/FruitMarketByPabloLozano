﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using FruitMarketAPI.Models;
using MarketCommon.Interfaces;
using MarketCommon.Models;

namespace FruitMarketAPI.Controllers
{
    public class SalesController : ApiController
    {
        IMarketRepository _repository;

        public SalesController(IMarketRepository repo)
        {
            _repository = repo;
        }

        // GET api/values
        public IEnumerable<MarketSale> Get()
        {
            return _repository.GetSales();
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]SaleDTO value)
        {
            MarketSale ms = new MarketSale();
            ms.ID = 0;
            ms.Quantity = int.Parse(value.Quantity);
            Fruit f = _repository.GetFruits().First(u => u.Name == value.FruitName);
            ms.SaleFruit = f;

            _repository.AddSale(ms);
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
