﻿using MarketCommon.Interfaces;
using MarketCommon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace FruitMarketAPI.Controllers
{
    public class FruitController : ApiController
    {
        IMarketRepository _repository;

        public FruitController(IMarketRepository repo)
        {
            _repository = repo;
        }

        // GET api/values
        public IEnumerable<Fruit> Get()
        {
            return _repository.GetFruits();
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]MarketSale value)
        {
            _repository.AddSale(value);
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

    }
}
