﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FruitMarketAPI.Models
{
    public class SaleDTO
    {
        public string Quantity
        { get; set; }

        public string FruitName
        { get; set; }
    }
}