﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FruitCommon
{
    public interface IFruitRepository
    {
        bool AddFruit();

        bool DeleteFruit();
    }
}
