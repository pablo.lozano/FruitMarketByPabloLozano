﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MarketCommon.Models;

namespace MarketCommon.Interfaces
{
    public interface IMarketRepository
    {
        IEnumerable<MarketSale> GetSales();

        IEnumerable<Fruit> GetFruits();

        bool AddSale(MarketSale sale);
    }
}
