﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarketCommon.Models
{
    public class Fruit
    {
        #region Properties

        public int ID
        { get; set; }

        public string Name
        { get; set; }

        public decimal Price
        { get; set; }

        #endregion
    }
}
