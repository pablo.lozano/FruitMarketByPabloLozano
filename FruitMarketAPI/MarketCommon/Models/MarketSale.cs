﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarketCommon.Models
{
    public class MarketSale
    {
        #region Properties

        public int ID
        { get; set; }

        public DateTime SaleDate
        { get; set; }

        public int Quantity
        { get; set; }

        public Fruit SaleFruit
        { get; set; }

        #endregion
    }
}
